(*
 * HTTP server for key/value pairs.
 *
 * It holds two areas:
 *     /system/ - read-only system information
 *     /data/   - read/write data
 *
 * The key `keys` has special meaning, when read it returns a list of all keys in the area.
 *
 * Examples
 *
 * % curl localhost:8080/system/keys
 * (Ok(StrLst(arch os os-version)))
 * % curl localhost:8080/system/arch
 * (Ok(String x86_64))
 * % curl localhost:8080/data/keys
 * (Ok(StrList()))
 * % curl -d "(String bar)" -X PUT localhost:8080/data/foo
 * (Ok"Value set")
 * % curl localhost:8080/data/keys
 * (Ok(StrLst(foo)))
 * % curl localhost:8080/data/foo
 * (Ok(String bar))
 *
 * The return format is S-expressions and key names are case sensitive.
 *)

open Core_kernel.Std

type store_entry =
    | StrLst of string list
    | String of string
    | Int of int
    with sexp

type 'a qresult = ('a, string) Result.t with sexp

module SystemData = struct
    module M = String.Map

    let system_info =
        let si_map = M.of_alist
            [ ("os", String "GNU/Linux")
            ; ("os-version", String "3.18.6")
            ; ("arch", String "x86_64")
            ]
        in
        match si_map with
        | `Ok m -> m
        | _ -> M.empty

    let all_keys = M.keys system_info

    let find_key k = match M.find system_info k with
        | None -> Error "Unknown key"
        | Some v -> Ok v
end

module RWData = struct
    module M = String.Map

    let rwdata : store_entry M.t ref = ref M.empty

    let all_keys () = M.keys !rwdata

    let find_key k = match M.find !rwdata k with
        | None -> Error "Unknown key"
        | Some v -> Ok v

    let set_key k v = rwdata := M.add !rwdata k v
end

module Main (C: V1_LWT.CONSOLE) (S: Cohttp_lwt.Server) = struct
    let success_sexp s = Ok s |> sexp_of_qresult String.sexp_of_t |> Sexp.to_string
    let failure_sexp s = Error s |> sexp_of_qresult String.sexp_of_t |> Sexp.to_string

    let handle_get req =
        let req_path = S.Request.uri req |> Uri.path in
        match String.split req_path '/' |> List.tl with
        | Some ["system"; "keys"] ->
                let val_str = Ok (StrLst SystemData.all_keys) |> sexp_of_qresult sexp_of_store_entry |> Sexp.to_string in
                S.respond_string `OK val_str ()
        | Some ("system"::k) ->
                let key_str = String.concat ~sep:"/" k in
                let val_str = SystemData.find_key key_str |> sexp_of_qresult sexp_of_store_entry |> Sexp.to_string in
                S.respond_string `OK val_str ()
        | Some ["data"; "keys"] ->
                let val_str = Ok (StrLst (RWData.all_keys ())) |> sexp_of_qresult sexp_of_store_entry |> Sexp.to_string in
                S.respond_string `OK val_str ()
        | Some ("data"::k) ->
                let key_str = String.concat ~sep:"/" k in
                let val_str = RWData.find_key key_str |> sexp_of_qresult sexp_of_store_entry |> Sexp.to_string in
                S.respond_string `OK val_str ()
        | _ -> S.respond_string `OK (failure_sexp "bad path") ()

    let handle_put req body =
        let req_path = S.Request.uri req |> Uri.path in
        match String.split req_path '/' |> List.tl with
        | Some ("data"::k) ->
                let key_str = String.concat ~sep:"/" k in
                lwt b = Cohttp_lwt_body.to_string body in
                RWData.set_key key_str (Sexp.of_string b |> store_entry_of_sexp);
                S.respond_string `OK (success_sexp "Value set") ()
        | _ -> S.respond_string `OK (failure_sexp "bad path") ()

    let bad_method_res req =
        let req_as_string = S.Request.sexp_of_t req |> Sexp.to_string in
        S.respond_string `Method_not_allowed (Printf.sprintf "Unsupported method! %s" req_as_string) ()

    let start c http =
        let callback conn_id req body =
            match S.Request.meth req with
            | `GET -> handle_get req
            | `PUT -> handle_put req body
            | _ -> bad_method_res req
        in
        let conn_closed (_, conn_id) =
            C.log c (Printf.sprintf "connexion closed");
            ()
        in
        let spec = S.make ~callback ~conn_closed () in

        http spec
end
