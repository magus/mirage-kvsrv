open Mirage

let httpsrv =
    let mode = `TCP (`Port 8080) in
    let server =
        match get_mode () with
        | `Xen -> conduit_direct (direct_stackv4_with_dhcp default_console tap0)
        | _ -> conduit_direct (socket_stackv4 default_console [Ipaddr.V4.any])
    in
    http_server mode server

let main =
    let packages = ["core_kernel"] in
    let libraries = ["core_kernel"; "sexplib.syntax"] in
    foreign ~libraries ~packages "Server.Main" (console @-> http @-> job)

let () = register "kvsrv" [main $ default_console $ httpsrv]
